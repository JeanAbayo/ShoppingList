class BaseConfig(object):
	DEBUG = False
	SECRET_KEY = '&\xb2\xc8\x80^H\xef\xb7\xc9\xb11\\\xf0\xe5}\xdd\xb8[O\x0b\tK\x0e\xbe'
	SQLALCHEMY_DATABASE_URI = "sqlite:///posts.db"
	SQLALCHEMY_TRACK_MODIFICATIONS = True

class DevConfig(BaseConfig):
	DEBUG = True

class ProdConfig(BaseConfig):
	DEBUG = False