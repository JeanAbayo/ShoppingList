from flask import Flask, render_template, redirect, url_for, request, session, flash, g, jsonify
from functools import wraps

from flask_sqlalchemy import SQLAlchemy

import sqlite3

app = Flask(__name__)

# Configurations
import os
app.config.from_object(os.environ['APP_SETTINGS'])

db = SQLAlchemy(app)

from models import *

def login_required(f):
	@wraps(f)
	def wrap(*args, **kwargs):
		if 'logged_in' in session:
			return f(*args, **kwargs)
		else:
			flash('Please Log into your ShoppingList Account first')
			return redirect(url_for('login'))
	return wrap

@app.route('/')
def home():
	return render_template("homepage.html")

@app.route('/dashboard')
@login_required
def dashboard():

	lists_data = db.session.query(ShoppingLists).all()

	return render_template("dashboard.html", all_lists=lists_data)

@app.route('/login', methods=['GET', 'POST'])
def login():
	error = None
	if request.method == 'POST':
		if request.form['email'] != 'test@test.com' or request.form['password'] != 'admin':
			error = 'Invalid credentials'
		else:
			session['logged_in'] = True
			flash('Welcome to your ShoppingList Account')
			return redirect(url_for('dashboard'))
	return render_template('login.html', error = error)

@app.route('/logout')
@login_required
def logout():
	session.pop('logged_in', None)
	flash('We hope you enjoyed organizing and sharing list see you soon')
	return redirect(url_for('home'))

@app.route('/create_user')
def create_user():
	# return request

    return jsonify(username='John',
                   email='Doe',
                   id='1')

def connect_db():
	return sqlite3.connect('posts.db')

if __name__ == '__main__':
	app.run()