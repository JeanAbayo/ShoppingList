![Alt text](https://travis-ci.org/JeanAbayo/ShoppingList.svg?branch=master "TravisCI status")
[![Coverage Status](https://coveralls.io/repos/github/JeanAbayo/ShoppingList/badge.svg?branch=master)](https://coveralls.io/github/JeanAbayo/ShoppingList?branch=master)
<a href="https://happyshoppinglist.herokuapp.com/">
    <img src="static/images/sl_logo.png" alt="ShoppingList logo" title="ShoppingList" align="right" height="60" />
</a>
<a href="https://codeclimate.com/github/JeanAbayo/ShoppingList/coverage"><img src="https://codeclimate.com/github/JeanAbayo/ShoppingList/badges/coverage.svg" /></a>

ShoppingList
======================

ShoppingList makes shopping quick, easy and fun allowing users to add, update, view or delete items in a shopping list and share the lists with the public.

