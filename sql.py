import sqlite3

with sqlite3.connect("sample.db") as connection:
	c = connection.cursor()
	c.execute("DROP TABLE lists")
	c.execute("CREATE TABLE lists(title TEXT, description TEXT)")
	c.execute('INSERT INTO lists VALUES("Groceries", "All kinds of them")')
	c.execute('INSERT INTO lists VALUES("School Materials", "Bags and stuff")')