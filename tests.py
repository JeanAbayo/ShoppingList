from app import app
import unittest

class FlaskTestCase(unittest.TestCase):

	def test_index(self):
		tester = app.test_client(self)
		response = tester.get('/login', content_type='html/text')
		self.assertEqual(response.status_code, 200)

	def test_homepage_loading(self):
		tester = app.test_client(self)
		response = tester.get('/', content_type='html/text')
		self.assertTrue(b'Keep track of your shopping' in response.data)

	def test_correct_login(self):
		tester = app.test_client(self)
		response = tester.post(
			'/login',
			data=dict(email="test@test.com", password="admin"),
			follow_redirects=True
		)
		self.assertIn(b'Welcome to your ShoppingList Account', response.data)

	def test_incorrect_login(self):
		tester = app.test_client(self)
		response = tester.post(
			'/login',
			data=dict(email="Wrong", password="Wrong"),
			follow_redirects=True
			)
		self.assertIn(b'Invalid credentials', response.data)

	def test_logout(self):
		tester = app.test_client(self)
		tester.post(
			'/login',
			data=dict(email="test@test.com", password="admin"),
			follow_redirects=True
		)
		response = tester.get(
			'/logout',
			follow_redirects=True
		)
		self.assertIn(
			b'We hope you enjoyed organizing and sharing list see you soon',
			response.data
		)

	def test_dashboard_login_first(self):
		tester = app.test_client(self)
		response = tester.get(
			'/dashboard',
			follow_redirects=True
		)
		self.assertIn(b'Please Log into your ShoppingList Account first', response.data)

	def test_login_to_logout(self):
		tester = app.test_client(self)
		response_non_logged_in = response_logged_in = tester.get(
			'/',
			follow_redirects=True
		)
		response_logged_in = tester.post(
			'/login',
			data=dict(email="test@test.com", password="admin"),
			follow_redirects=True
		)		
		self.assertFalse(b'Logout' in response_non_logged_in.data)
		self.assertTrue(b'Logout' in response_logged_in.data)


if __name__ == '__main__':
	unittest.main()