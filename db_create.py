from app import db

from models import ShoppingLists

db.create_all()

db.session.add(ShoppingLists("Lorem ipsum.", "Lorem ipsum dolor."))
db.session.add(ShoppingLists("Lorem.", "Lorem ipsum dolor sit."))

db.session.commit()